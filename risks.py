from datetime import datetime


def risk_by_age(age):
    if age < 30:
        return -2

    if age >= 30 and age <= 40:
        return -1
    return 0


def risk_by_income(income):
    if income > 200000:
        return -1
    else:
        return 0


def risk_by_house(_input):
    if "houses" in _input:
        for h in _input["houses"]:
            if h["ownership_status"] == "mortgaged":
                return -1
    return 0


def risk_by_dependents(dependents):

    if dependents:
        return 1
    return 0


def risk_by_marital(marital_status):

    if marital_status == "married":
        return 1
    return 0


def risk_by_mortgaged(ownership_status):
    if ownership_status == "mortgaged":
        return 1
    return 0


def risk_by_qtd_houses(houses):
    if len(houses) == 1:
        return 1
    return 0


def risk_by_qtd_vehicles(vehicles):
    if len(vehicles) == 1:
        return 1
    return 0


def risk_by_vehicle_year(year):
    if datetime.now().year - year <= 5:
        return 1
    return 0
