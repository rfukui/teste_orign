# Teste ORIGN v.0.0.5

**Este é um teste escrito como parte do processo de seleção ORIGN**
**Totalmente escrito em Python versão 3.6 + Framework FLASK**

## Requerimentos de instalação para executar o programa:
Sugestão de sequência de instalação com virtualenv já instalado na máquina.
Na pasta da aplicação execute
```
$ virtualenv .venv --python=python3
$ source .venv/bin/activate
$ pip install -r requirements.txt
$ chmod +x main.py
$ ./main.py
```
Ele criará um endpoint em http://localhost:8080/

Exemplo de uso:

```
$python
>>> j = {
    "age": 35,
    "dependents": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
>>>import requests
>>> r = requests.post("http://localhost:8080",json=j)
>>> r.json()
```


## Sugestões para o teste

#### Alteração do response
- Atualmente se recebe `houses` e `vehicles` e se responde com `home` e `auto`. Isso pode gerar um erro no desenvolvimento e de espectativa, normalmente se reponde com o que foi enviado.

#### Gravar os requests
- Este teste foi feito em FLASK pois para uma versão apenas de recebimento e envio de dados ele se comporta muito mais rápido que o Django. Salvando os requests e/ou pelo menos informações que se julgam importantes, seria possível com o tempo também avaliar a capacidade de modelagem de dados de quem está escrevendo.

#### Umbrella
- O cálculo foi feito como informado, apenas "somando" o questionário de risco, mas o teste leva a intuir que a idade e o income fazem parte deste cálculo, uma vez que ele pode ser (pelo menos no meu entendimento) uma linha de seguro.

#### Mais de um endpoint
- Com apenas um endpoint a pessoa pode esquecer de componentizar melhor o sistema. Com isso consegue-se perceber também a capacidade da pessoa de reaproveitar código, recebendo apenas uma das informações em endpoints diferentes para se ter o retorno individual dos cálculos.
