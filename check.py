
def check_input(_input):
    positive_int_fields = ["age", "dependents", "income"]
    if type(_input) != dict:
        return {"ERROR": "you must be post a json"}
    for field in positive_int_fields:
        if field not in _input or type(_input[field]) != int or _input[field] < 0:
            msg = "the %s is required and must be a positive integer" % field
            return {"ERROR": msg}

    if "marital_status" not in _input or _input["marital_status"] not in ["single", "married"]:
        return {"ERROR": "the marital_status is required and the expected value is 'single' or 'married'"}

    if "risk_questions" in _input:
        chk_risk = check_risk(_input["risk_questions"])
        if len(chk_risk):
            return chk_risk
    else:
        return {"ERROR": "risk_questions is required"}

    if "houses" in _input:
        chk_houses = check_houses(_input['houses'])
        if len(chk_houses):
            return chk_houses

    if "vehicles" in _input:
        chk_vehicles = check_vehicles(_input["vehicles"])
        if len(chk_vehicles):
            return check_houses

    return {}


def check_vehicles(vehicles):
    if type(vehicles) != list:
        return {"ERROR": "vehicles must be a list"}
    for v in vehicles:
        if "key" not in v or type(v["key"]) != int or v["key"] < 0:
            return {"ERROR": "key is required in vehicles and must be a positive integer"}

        if "year" not in v or type(v["year"]) != int or v["year"] < 0:
            return {"ERROR": "year is required and the expected value is a positive int"}
    return {}


def check_houses(houses):
    if type(houses) != list:
        return {"ERROR": "houses must be a list"}
    for h in houses:
        if "key" not in h or type(h["key"]) != int or h["key"] < 0:
            return {"ERROR": "key is required in houses and must be a positive integer"}
    
        if "ownership_status" not in h or type(h["ownership_status"]) != str and h["ownership_status"] not in ["owned", "mortgaged"]:
            return {"ERROR": "ownership_status is required and the expected value is 'owned' or 'mortgaged'"}
    return {}


def check_risk(r):
    if type(r)!=list or len(r) != 3:
        return {"ERROR": "risk_questions must be a array with 3 bools"}
    else:
        for x in r:
            if x not in [0, 1]:
                return {"ERROR": "risk_questions must be a array with 3 bool " + str(x) + " i'snt 0 or 1"}
    return {}
