from risks import *

def calculated_risk(risk):
    if risk <= 0:
        return "economic"
    if risk > 0 and risk < 3:
        return "regular"
    else:
        return "responsible"


def calc_disability(_input):
    risk = sum(_input["risk_questions"])
    if _input["income"] <= 0 or _input["age"] > 60:
        return "ineligible"
    else:
        risk += risk_by_age(_input["age"])
        risk += risk_by_income(_input["income"])
        risk += risk_by_house(_input)
        risk += risk_by_dependents(_input["dependents"])
        risk -= risk_by_marital(_input["marital_status"])
        return calculated_risk(risk)


def calc_life(_input):
    risk = sum(_input["risk_questions"])
    if _input["age"] > 60:
        return "ineligible"
    else:
        risk += risk_by_age(_input["age"])
        risk += risk_by_income(_input["income"])
        risk += risk_by_dependents(_input["dependents"])
        risk += risk_by_marital(_input["marital_status"])
        return calculated_risk(risk)


def calc_house(house, _input):
    risk = sum(_input["risk_questions"])
    risk += risk_by_age(_input["age"])
    risk += risk_by_income(_input["income"])
    risk += risk_by_mortgaged(house["ownership_status"])
    risk += risk_by_qtd_houses(_input["houses"])

    return{"key": house["key"], "value": calculated_risk(risk)}


def calc_all_houses(_input):
    if "houses" in _input:
        return_houses = []
        for h in _input["houses"]:
            return_houses.append(calc_house(h, _input))
        return return_houses
    return "ineligible"


def calc_vehicle(vehicle, _input):
    risk = sum(_input["risk_questions"])
    risk += risk_by_age(_input["age"])
    risk += risk_by_income(_input["income"])
    risk += risk_by_qtd_vehicles(_input["vehicles"])
    risk += risk_by_vehicle_year(vehicle["year"])

    return{"key": vehicle["key"], "value": calculated_risk(risk)}


def calc_all_vehicles(_input):
    if "vehicles" in _input:
        return_vehicles = []
        for h in _input["vehicles"]:
            return_vehicles.append(calc_vehicle(h, _input))
        return return_vehicles
    return "ineligible"


def calc_umbrella(_input, insurances):
    risk = sum(_input["risk_questions"])
    if "economic" in [insurances["life"], insurances["disability"], ]:
        return calculated_risk(risk)

    if type(insurances["auto"]) == list:
        for v in insurances["auto"]:
            if v["value"] == "economic":
                return calculated_risk(risk)
    if type(insurances["home"]) == list:
        for h in insurances["home"]:
            if h["value"] == "economic":
                return calculated_risk(risk)

    return "ineligible"
