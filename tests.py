import unittest
import check
import risks
import calc
ALL_FIELDS_TEST = {
    "age": 35,
    "dependents": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NO_INCOME_TEST = {
    "age": 35,
    "dependents": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NO_AGE_TEST = {
    "dependents": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NEGATIVE_AGE_TEST = {
    "dependents": 2,
    "age": -35,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NO_DEPENDENTS_TEST = {
    "age": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NEGATIVE_DEPENDENTS_TEST = {
    "age": 2,
    "dependents": -35,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}

NO_VEHICLES_TEST = {
    "age": 35,
    "dependents": 2,
    "houses": [
        {"key": 0, "ownership_status": "owned"},
        {"key": 1, "ownership_status": "mortgaged"}
    ],
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],

}

NO_HOUSES_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
INVALID_HOUSES_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "houses": [
        {"key": 0, "year": 2018}
    ]
}
INVALID_VEHICLES_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": "2018"}
    ]
}
INVALID_RISK_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "risk_questions": "[0, 1, 0]",
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
INVALID_RISK_TEST2 = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "risk_questions": [0, 1, 2],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}
NO_RISK_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "married",
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}

INVALID_MARITAL_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "marital_status": "marroed",
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}

NO_MARITAL_TEST = {
    "age": 35,
    "dependents": 2,
    "income": 0,
    "risk_questions": [0, 1, 0],
    "vehicles": [
        {"key": 0, "year": 2018}
    ]
}

UMBRELLA = {
    "auto": [
        {
            "key": 0,
            "value": "regular"
        }
    ],
    "disability": "ineligible",
    "home": [
        {
            "key": 0,
            "value": "economic"
        },
        {
            "key": 1,
            "value": "regular"
        }
    ],
    "life": "regular",
    "umbrella": "regular"
}


class TestCheckMethods(unittest.TestCase):

    def test_check_input(self):
        self.assertEqual(check.check_input(ALL_FIELDS_TEST), {})

    def test_check_input_no_age(self):
        self.assertEqual(check.check_input(NO_AGE_TEST), {
                         'ERROR': 'the age is required and must be a positive integer'})

    def test_check_input_negative_age(self):
        self.assertEqual(check.check_input(NEGATIVE_AGE_TEST), {
                         'ERROR': 'the age is required and must be a positive integer'})

    def test_check_input_no_dependents(self):
        self.assertEqual(check.check_input(NO_DEPENDENTS_TEST), {
                         'ERROR': 'the dependents is required and must be a positive integer'})

    def test_check_input_negative_dependents(self):
        self.assertEqual(check.check_input(NEGATIVE_DEPENDENTS_TEST), {
                         'ERROR': 'the dependents is required and must be a positive integer'})

    def test_check_input_no_income(self):
        self.assertEqual(check.check_input(NO_INCOME_TEST), {
                         'ERROR': 'the income is required and must be a positive integer'})

    def test_check_input_negative_dependents(self):
        self.assertEqual(check.check_input(NEGATIVE_DEPENDENTS_TEST), {
                         'ERROR': 'the dependents is required and must be a positive integer'})

    def test_check_input_no_vehicles(self):
        self.assertEqual(check.check_input(NO_VEHICLES_TEST), {})

    def test_check_input_no_houses(self):
        self.assertEqual(check.check_input(NO_HOUSES_TEST), {})

    def test_check_input_no_houses(self):
        self.assertEqual(check.check_input(INVALID_HOUSES_TEST), {
                         "ERROR": "ownership_status is required and the expected value is 'owned' or 'mortgaged'"})

    def test_check_input_no_marital(self):
        self.assertEqual(check.check_input(NO_MARITAL_TEST), {
                         "ERROR": "the marital_status is required and the expected value is 'single' or 'married'"})

    def test_check_input_invalid_marital(self):
        self.assertEqual(check.check_input(INVALID_MARITAL_TEST), {
                         "ERROR": "the marital_status is required and the expected value is 'single' or 'married'"})

    def test_check_risk_invalid(self):
        self.assertEqual(check.check_input(INVALID_RISK_TEST), {
                         "ERROR": "risk_questions must be a array with 3 bools"})

    def test_check_no_risk(self):
        self.assertEqual(check.check_input(NO_RISK_TEST), {
                         "ERROR": "risk_questions is required"})

    def test_check_risk_invalid2(self):
        self.assertEqual(check.check_input(INVALID_RISK_TEST2), {
                         'ERROR': "risk_questions must be a array with 3 bool 2 i'snt 0 or 1"})


class TestRiskMethods(unittest.TestCase):

    def test_risk_age1(self):
        self.assertEqual(risks.risk_by_age(29), -2)

    def test_risk_age2(self):
        self.assertEqual(risks.risk_by_age(30), -1)

    def test_risk_age3(self):
        self.assertEqual(risks.risk_by_age(40), -1)

    def test_risk_age4(self):
        self.assertEqual(risks.risk_by_age(41), 0)

    def test_risk_income1(self):
        self.assertEqual(risks.risk_by_income(0), 0)

    def test_risk_income2(self):
        self.assertEqual(risks.risk_by_income(200000), 0)

    def test_risk_income3(self):
        self.assertEqual(risks.risk_by_income(400000), -1)

    def test_risk_by_house1(self):
        self.assertEqual(risks.risk_by_house(ALL_FIELDS_TEST), -1)

    def test_risk_by_house2(self):
        self.assertEqual(risks.risk_by_house(NO_HOUSES_TEST), 0)

    def test_risk_by_dependents1(self):
        self.assertEqual(risks.risk_by_dependents(0), 0)

    def test_risk_by_dependents2(self):
        self.assertEqual(risks.risk_by_dependents(3), 1)

    def test_risk_by_marital1(self):
        self.assertEqual(risks.risk_by_marital("married"), 1)

    def test_risk_by_marital2(self):
        self.assertEqual(risks.risk_by_marital("single"), 0)

    def test_risk_by_mortgaged1(self):
        self.assertEqual(risks.risk_by_mortgaged("mortgaged"), 1)

    def test_risk_by_mortgaged2(self):
        self.assertEqual(risks.risk_by_mortgaged("owned"), 0)

    def test_risk_by_houses1(self):
        self.assertEqual(risks.risk_by_qtd_houses(
            ALL_FIELDS_TEST["houses"]), 0)

    def test_risk_by_houses2(self):
        self.assertEqual(risks.risk_by_qtd_houses(
            [{"ownership_status": "owned", "key": 0}]), 1)

    def test_risk_by_vehicles1(self):
        self.assertEqual(risks.risk_by_qtd_vehicles(
            [{"year": 2001, "key": 0}]), 1)

    def test_risk_by_vehicles2(self):
        self.assertEqual(risks.risk_by_qtd_vehicles(
            [{"year": 2001, "key": 0}, {"year": 2001, "key": 1}]), 0)

    def test_risk_by_vehicle_year1(self):
        self.assertEqual(risks.risk_by_vehicle_year(2012), 0)

    def test_risk_by_vehicle_year2(self):
        self.assertEqual(risks.risk_by_vehicle_year(2013), 1)

    def test_risk_by_vehicle_year3(self):
        self.assertEqual(risks.risk_by_vehicle_year(2014), 1)


class TestCalcMethods(unittest.TestCase):

    def test_calculated_risk1(self):
        self.assertEqual(calc.calculated_risk(-1), "economic")

    def test_calculated_risk2(self):
        self.assertEqual(calc.calculated_risk(0), "economic")

    def test_calculated_risk3(self):
        self.assertEqual(calc.calculated_risk(1), "regular")

    def test_calculated_risk4(self):
        self.assertEqual(calc.calculated_risk(4), "responsible")

    def test_disability1(self):
        self.assertEqual(calc.calc_disability(ALL_FIELDS_TEST), "ineligible")

    def test_disability2(self):
        ALL_FIELDS_TEST["income"] = 200
        self.assertEqual(calc.calc_disability(ALL_FIELDS_TEST), "economic")

    def test_disability3(self):
        ALL_FIELDS_TEST["income"] = 200
        ALL_FIELDS_TEST["age"] = 70
        self.assertEqual(calc.calc_disability(ALL_FIELDS_TEST), "ineligible")

    def test_life1(self):
        ALL_FIELDS_TEST["age"] = 50
        self.assertEqual(calc.calc_life(ALL_FIELDS_TEST), "responsible")

    def test_life2(self):
        ALL_FIELDS_TEST["age"] = 35
        self.assertEqual(calc.calc_life(ALL_FIELDS_TEST), "regular")

    def test_life3(self):
        ALL_FIELDS_TEST["income"] = 300000
        ALL_FIELDS_TEST["age"] = 15
        self.assertEqual(calc.calc_life(ALL_FIELDS_TEST), "economic")

    def test_life4(self):
        ALL_FIELDS_TEST["age"] = 70
        self.assertEqual(calc.calc_life(ALL_FIELDS_TEST), "ineligible")

    def test_calc_house1(self):
        ALL_FIELDS_TEST["age"] = 35
        ALL_FIELDS_TEST["income"] = 0
        house = {"key": 0, "ownership_status": "owned"}
        self.assertEqual(calc.calc_house(house, ALL_FIELDS_TEST), {
                         'key': 0, 'value': 'economic'})

    def test_calc_house2(self):
        ALL_FIELDS_TEST["age"] = 35
        ALL_FIELDS_TEST["income"] = 0
        house = {"key": 0, "ownership_status": "mortgaged"}
        self.assertEqual(calc.calc_house(house, ALL_FIELDS_TEST), {
                         'key': 0, 'value': 'regular'})

    def test_calc_all_house1(self):
        self.assertEqual(calc.calc_all_houses(ALL_FIELDS_TEST), [
                         {'key': 0, 'value': 'economic'}, {'key': 1, 'value': 'regular'}])

    def test_calc_vehicle1(self):
        ALL_FIELDS_TEST["age"] = 35
        ALL_FIELDS_TEST["income"] = 0
        vehicle = {"key": 0, "year": 2018}
        self.assertEqual(calc.calc_vehicle(vehicle, ALL_FIELDS_TEST), {
                         'key': 0, 'value': 'regular'})

    def test_calc_vehicle2(self):
        ALL_FIELDS_TEST["age"] = 35
        ALL_FIELDS_TEST["income"] = 0
        ALL_FIELDS_TEST["vehicles"] = [
            {"key": 0, "year": 2018}, {"key": 1, "year": 2005}]
        vehicle = {"key": 1, "year": 2005}
        self.assertEqual(calc.calc_vehicle(vehicle, ALL_FIELDS_TEST), {
                         'key': 1, 'value': 'economic'})

    def test_calc_all_vehicle1(self):
        ALL_FIELDS_TEST["vehicles"] = [
            {"key": 0, "year": 2018}, {"key": 1, "year": 2005}]
        self.assertEqual(calc.calc_all_vehicles(ALL_FIELDS_TEST), [
                         {'key': 0, 'value': 'regular'}, {'key': 1, 'value': 'economic'}])

    def test_calc_umbrella1(self):
        self.assertEqual(calc.calc_umbrella(
            ALL_FIELDS_TEST, UMBRELLA), "regular")

    def test_calc_umbrella2(self):
        UMBRELLA["home"] = [
            {
                "key": 0,
                "value": "regular"
            },
            {
                "key": 1,
                "value": "regular"
            }]
        self.assertEqual(calc.calc_umbrella(
            ALL_FIELDS_TEST, UMBRELLA), "ineligible")


if __name__ == "__main__":
    unittest.main()
