#!.venv/bin/python
from flask import Flask, jsonify, render_template, request
from waitress import serve
app = Flask(__name__)
app.config.from_object(__name__)
import os
from check import check_input
from calc import calc_disability, calc_life, calc_umbrella, calc_all_houses, calc_all_vehicles


@app.route("/", methods=['POST'])
def index():

    _input = request.get_json()

    chk_input = check_input(_input)
    if len(chk_input):
        return jsonify(chk_input)

    disability = calc_disability(_input)
    life = calc_life(_input)
    houses = calc_all_houses(_input)
    vehicles = calc_all_vehicles(_input)
    insurances = {"disability": disability,
                  "life": life,
                  "home": houses,
                  "auto": vehicles
                  }
    insurances["umbrella"] = calc_umbrella(_input, insurances)
    return jsonify(insurances,
                   )


app.config.from_envvar('FLASKR_SETTINGS', silent=True)

if __name__ == "__main__":
    serve(app)
